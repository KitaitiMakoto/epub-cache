use std::process;
use epub_cache;

fn main() {
    let epub_path = std::env::args().nth(1).unwrap_or_else(|| {
        eprintln!("No EPUB file specified");
        process::exit(1);
    });
    let cache_path = epub_cache::cache_path_from_epub_path(&epub_path)
        .unwrap_or_else(|err| {
            eprintln!("{}", err);
            process::exit(1);
        });
    println!("{}", cache_path.to_string_lossy());
}

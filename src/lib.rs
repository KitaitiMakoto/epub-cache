use std::error::Error;
use std::path::{Path,PathBuf};
use epub::doc::EpubDoc;
use base64;
use directories::ProjectDirs;

const DIR_NAME_CHUNK_SIZE: usize = 4;

pub fn cache_path_from_epub_path<P: AsRef<Path>>(path: P) -> Result<PathBuf, Box<dyn Error>> {
    let doc = EpubDoc::new(path).map_err(|_err| "Not an EPUB")?;
    let modified = doc.mdata("dcterms:modified").ok_or("dcterms:modified not found")?;
    let unique_identifier = &doc.unique_identifier.ok_or("Unique Identifier not found")?;
    cache_path_from_unique_identifier_and_modified(unique_identifier, &modified)
}

pub fn cache_path_from_unique_identifier_and_modified(unique_identifier: &str, modified: &str) -> Result<PathBuf, Box<dyn Error>> {
    let project_dirs = ProjectDirs::from("net.kitaitimakoto.epub-cache", "", "epub-cache").ok_or("Cache directory not available")?;
    let epub_dir = dir_from_unique_identifier(unique_identifier);
    Ok(project_dirs.cache_dir()
       .join(epub_dir)
       .join(format!("{}.epub", modified)))
}

// Core algorithm. Stability is desired.
fn dir_from_unique_identifier(unique_identifier: &str) -> PathBuf {
    let dir_name = base64::encode_config(unique_identifier, base64::URL_SAFE_NO_PAD);
    let mut dir = PathBuf::new();
    for chunk in dir_name.as_bytes().chunks(DIR_NAME_CHUNK_SIZE) {
        dir.push(&chunk.iter().map(|&s| s as char).collect::<String>())
    }
    dir
}

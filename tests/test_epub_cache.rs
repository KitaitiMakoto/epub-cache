use std::path;
use directories::ProjectDirs;
use epub_cache;

#[test]
fn test_cache_path_from_unique_identifier_and_modified() {
    let project_dir = ProjectDirs::from("net.kitaitimakoto.epub-cache", "", "epub-cache").unwrap();
    let unique_identifier = "urn:uuid:f975b8a5-5ab6-40d1-946f-ddc3f12bbbfe";
    let modified = "2019-04-19T09:00:00Z";

    let dir_name = vec!["dXJu", "OnV1", "aWQ6", "Zjk3", "NWI4", "YTUt", "NWFi", "Ni00", "MGQx", "LTk0", "NmYt", "ZGRj", "M2Yx", "MmJi", "YmZl"].join(&path::MAIN_SEPARATOR.to_string());
    let expected = format!("{}/{}/{}.epub",
			   project_dir.cache_dir().to_string_lossy(),
			   dir_name,
			   modified);
    let actual = epub_cache::cache_path_from_unique_identifier_and_modified(unique_identifier, modified).unwrap();
    assert_eq!(expected, actual.to_string_lossy());
}
